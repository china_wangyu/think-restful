<?php
/**
 * Created by User: wene<china_wangyu@aliyun.com> Date: 2019/4/3 Time: 16:36
 */

namespace app\api\controller\v1;

use think\restful\jwt\Jwt;

/**
 * Class Reflex API基类
 * @package app\api\controller\v1
 */
class Base extends \think\restful\Api
{
    /**
     * Base constructor. 有什么事要在父类执行之后执行的代码，请写在parent::__construct($debug);下
     * @param bool $debug
     */
    public function __construct($debug = false)
    {
        parent::__construct($debug);
    }

    /**
     * 继承父类方法，如果有什么要在最开始执行的，请写在里面
     */
    protected function handle()
    {
        if ($this->config['API_AUTHORIZATION']){
            // 开启JWT验证,执行业务代码
            if(!isset($this->param['jwt']) or !isset($this->param['signature'])) {
                // 没有jwt参数 或 signature 签名
                $this->error('400 缺少API授权信息~');
            }
            $jwtArr = Jwt::decode($this->param['jwt'],$this->config['API_AUTHORIZATION_KEY']);
            $userJwtSignature = md5(join(',',$jwtArr['data']));
            if ($userJwtSignature !== $this->param['signature']) {
                $this->error('400 API授权信息错误~');
            }
        }
    }
}