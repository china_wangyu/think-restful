<?php
/**
 * Created by User: wene<china_wangyu@aliyun.com> Date: 2019/4/12 Time: 14:32
 */

namespace think\restful\route;


use think\restful\Base;

/**
 * Class Reflex 路由注册模型
 * @package think\restful\route
 */
class Route extends Base
{
    public static $obj;
    public  $controller = 'Index';
    public  $action = 'index';
    public  $version = 'v1';
    public  $module = 'api';

    protected $header = [
        'Access-Control-Allow-Origin' => '*',
        'Access-Control-Allow-Methods' => 'GET', 'POST', 'PUT', 'DELETE', 'PATCH', 'HEAD', 'OPTIONS',
    ];

    public function __construct()
    {
        parent::__construct();
    }

    // 获取当前类对象
    public static function getObj(): Route
    {
        if (!isset(static::$obj))static::$obj = new static();
        return static::$obj;
    }

    /**
     * 注册路由模型
     * @param string $module
     */
    public static function register(string $module):void
    {
        $obj = static::getObj();
        $obj->module = $module;
        if (isset($_SERVER['PATH_INFO'])) {
            preg_match('/\\/v[\d+?]/i', $_SERVER['PATH_INFO'], $matchs);
            if (isset($matchs[0])) {
                $bool = $obj->setRoute();
                if ($bool) {
                    $obj->validate();
                    $obj->registerApiVersionRoute();
                }
            }
        }
    }

    /**
     * 设置路由参数
     * @return bool
     */
    public function setRoute()
    {
        $urlpare = explode('/', trim($_SERVER['PATH_INFO'], '\\/'));
        if (in_array($urlpare[0],$this->config['API_VERSION'])){
            $this->version = $urlpare[0];
            isset($urlpare[1]) && $this->controller = $urlpare[1];
            isset($urlpare[2]) && $this->action = $urlpare[2];
        }else{
            if ($this->config['API_MODULE'] != $urlpare[0]){
                return false;
            }
            $this->module = $urlpare[0];
            isset($urlpare[1]) && $this->version = $urlpare[1];
            isset($urlpare[2]) && $this->controller = $urlpare[2];
            isset($urlpare[3]) && $this->action = $urlpare[3];
        }
        return true;
    }

    public function validate()
    {
        $apiRouteValidate = new \think\restful\validate\Route();
        $apiRouteValidate->execute($this);
    }

    /**
     * 注册API版本路由
     */
    private function registerApiVersionRoute()
    {
        $urlpare = explode('/', trim($_SERVER['PATH_INFO'], '\\/'));
        $roule = function () {
            # 动态注册域名的路由规则
            \think\facade\Route::rule(':version/:controller',
                $this->module . '/:version.:controller/' . $this->action)
                ->header($this->header)->allowCrossDomain(true);
            \think\facade\Route::rule(':version/:controller/:function',
                $this->module . '/:version.:controller/:function')
                ->header($this->header)->allowCrossDomain(true);
        };
        in_array($urlpare[0],$this->config['API_VERSION']) ?
            $roule() :
            \think\facade\Route::group($this->module, function()use($roule){
                $roule();
            });
    }
}