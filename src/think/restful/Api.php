<?php
/**
 * Created by User: wene<china_wangyu@aliyun.com> Date: 2019/4/11 Time: 15:21
 */

namespace think\restful;

use think\restful\exception\ApiException;
use think\restful\validate\Authorize;
use think\restful\validate\Param;
use think\restful\response\Json;

/**
 * Class Reflex Api基类
 * @package think\restful
 */
abstract class Api extends Base
{
    /**
     * @var bool Base::$debug
     */
    protected $debug;

    public function __construct($debug = false)
    {
        parent::__construct();
        $this->debug = $debug;
        if (!$debug){
            $this->validate();
        }
    }

    // 污水处理
    private function validate()
    {
        try {
            $apiAuthorizeValidate = new Authorize();
            $apiAuthorizeValidate->execute();
            $apiParamValidate = new Param();
            $apiParamValidate->execute($this,$this->request->action());
        } catch (\Exception $exception) {
            ApiException::exception($exception->getMessage(),404);
        }
    }

    //  成功输出数据
    protected function success($msg,array $data = [])
    {
        return Json::json(200,$msg,$data);
    }

     // 失败输出数据
    protected function error($msg,array $data = [])
    {
        return Json::json(404,$msg,$data);
    }
}