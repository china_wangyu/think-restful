<?php
/**
 * Created by User: wene<china_wangyu@aliyun.com> Date: 2019/4/11 Time: 16:28
 */

namespace think\restful\response;

/**
 * Class Json API响应类
 * @package think\restful\response
 */
class Json
{
    /**
     * json 数据格式化
     * @param int $code 状态码
     * @param string $msg 消息
     * @param array $data 数据
     * @return array|null 返回 array 数据
     */
    public static function json(int $code, string $msg, array $data = []):?array
    {
        return [
            'responseCode' => $code,
            'responseMsg' => $msg,
            'responseData' => $data,
        ];
    }

    /**
     * 设置HTTP响应Header
     * @param int $code
     */
    public static function header(int $code = 200):void
    {
        http_response_code($code);
        $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.1');
        $get_response_text = static::http_response_text($code);
        header($protocol . ' ' . $code . ' ' . $get_response_text);
        header('Content-Type:application/json;charset=utf-8');
    }

    /**
     * 返回json响应
     * @param int $code  响应code
     * @param string $msg  响应msg
     * @param array $data  响应data
     */
    public static function response(int $code, string $msg, array $data = []): void
    {
        $http_response_data = static::json($code, $msg, $data);
        static::header($code);
        echo json_encode($http_response_data, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
        die;
    }

    /**
     * 获取 http_response_text
     * @param int $code
     * @return string
     */
    public static function http_response_text(int $code = 200):string
    {
        switch ($code) {
            case 100:
                $text = 'Continue';break;
            case 101:
                $text = 'Switching Protocols';break;
            case 200:
                $text = 'OK';break;
            case 201:
                $text = 'Created';break;
            case 202:
                $text = 'Accepted';break;
            case 203:
                $text = 'Non-Authoritative Information';break;
            case 204:
                $text = 'No Content';break;
            case 205:
                $text = 'Reset Content';break;
            case 206:
                $text = 'Partial Content';break;
            case 300:
                $text = 'Multiple Choices';break;
            case 301:
                $text = 'Moved Permanently';break;
            case 302:
                $text = 'Moved Temporarily';break;
            case 303:
                $text = 'See Other';break;
            case 304:
                $text = 'Not Modified';break;
            case 305:
                $text = 'Use Proxy';break;
            case 400:
                $text = 'Bad Request';break;
            case 401:
                $text = 'Unauthorized';break;
            case 402:
                $text = 'Payment Required';break;
            case 403:
                $text = 'Forbidden';break;
            case 404:
                $text = 'Not Found';break;
            case 405:
                $text = 'Method Not Allowed';break;
            case 406:
                $text = 'Not Acceptable';break;
            case 407:
                $text = 'Proxy Authentication Required';break;
            case 408:
                $text = 'Request Time-out';break;
            case 409:
                $text = 'Conflict';break;
            case 410:
                $text = 'Gone';break;
            case 411:
                $text = 'Length Required';break;
            case 412:
                $text = 'Precondition Failed';break;
            case 413:
                $text = 'Request Entity Too Large';break;
            case 414:
                $text = 'Request-URI Too Large';break;
            case 415:
                $text = 'Unsupported Media Type';break;
            case 500:
                $text = 'Internal Server Error';break;
            case 501:
                $text = 'Not Implemented';break;
            case 502:
                $text = 'Bad Gateway';
                break;
            case 503:
                $text = 'Service Unavailable';
                break;
            case 504:
                $text = 'Gateway Time-out';
                break;
            case 505:
                $text = 'HTTP Version not supported';
                break;
            default:
                $text = 'HTTP code not define service';
                break;
        }
        return $text;
    }
}