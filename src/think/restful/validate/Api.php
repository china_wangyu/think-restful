<?php


namespace think\restful\validate;


use think\restful\Base;
use think\restful\curl\Curl;

class Api extends Base
{
    public function __construct()
    {
        parent::__construct();
    }

    public function execute(string $host,string $method,array $param = []):Curl
    {
        // TODO: Implement execute() method.
        $curl = Curl::curl($method,$host,$param);
        return $curl;
    }
}