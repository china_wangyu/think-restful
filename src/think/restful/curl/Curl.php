<?php
/**
 * Created by User: wene<china_wangyu@aliyun.com> Date: 2019/4/12 Time: 14:58
 */

namespace think\restful\curl;

use think\restful\exception\ApiException;

/**
 * Class Curl API接口请求类（Curl/Curl类封装）
 * @package \think\restful\curl
 */
class Curl extends \Curl\Curl
{
    /**
     * 发起 CURL 请求
     * @param string $method    请求方式
     * @param string $url   请求连接
     * @param array $data   请求参数
     * @return \Curl\Curl
     */
    public function __invoke(string $method,string $url,array $data = []):\Curl\Curl
    {
        // TODO: Implement __invoke() method.
        try{
            $curl = new Curl();
            $curl->$method($url, $data);
            return $curl;
        }catch (\Exception $exception){
            ApiException::exception('404 '.$exception->getMessage());
        }
    }

    /**
     * 发起 CURL 请求
     * @param string $method    请求方式
     * @param string $url   请求连接
     * @param array $data   请求参数
     * @return \Curl\Curl
     */
    public static function curl(string $method,string $url,array $data = []):\Curl\Curl
    {
        try{
            $curl = new static();
            return $curl($method,$url,$data);
        }catch (\Exception $exception){
            ApiException::exception('404 '.$exception->getMessage());
        }
    }
}