<?php
/**
 * Created by User: wene<china_wangyu@aliyun.com> Date: 2019/4/11 Time: 14:59
 */

namespace think\restful;


use think\facade\Request;
use think\facade\Validate;

/**
 * Class Base 统一扩展的基类
 * @package think\restful
 */
abstract class Base
{
    /**
     * @var mixed $request \think\facade\Request::instance()
     */
    public $request;
    /**
     * @var array $param \think\facade\Request::param()
     */
    public $param;
    /**
     * @var mixed Base::$config
     */
    protected $config;

    /**
     * @var mixed $validate \think\facade\Validate::instance()
     */
    protected $validate;

    public function __construct()
    {

        $this->request = Request::instance();
        $this->param = $this->request->param();
        $this->config = config('api.');
        $this->validate = Validate::instance();
        $this->handle();
    }

    protected function handle(){}
}