# tp-rest-api

[![star](https://gitee.com/china_wangyu/think-restful/badge/star.svg?theme=dark)](https://gitee.com/china_wangyu/think-restful/stargazers)
[![fork](https://gitee.com/china_wangyu/think-restful/badge/fork.svg?theme=dark)](https://gitee.com/china_wangyu/think-restful/members)
[![Fork me on Gitee](https://gitee.com/china_wangyu/think-restful/widgets/widget_6.svg)](https://gitee.com/china_wangyu/think-restful)


#### 介绍
ThinkPHP 5.1的restful接口composer扩展

#### 软件架构
软件架构说明
~~~
think         模块目录
    ├─ restful        核心目录
        ├─ command              API命令行
            ├─ Check.php        Api接口自检命令行脚本
            ├─ Make.php         Api接口文档命令行脚本
        ├─ curl                 API 封装的CURL
            ├─ Curl.php         Api 封装的CURL请求类
        ├─ exception            exception 异常捕获封装
            ├─ ApiException.php Api 异常捕获封装类
        ├─ helper               exception 接口助手
            ├─ Helper.php       Api 接口助手工具类
        ├─ jwt                  JWT 接口授权
            ├─ Jwt.php          Api JWT接口授权类
        ├─ reflex               接口反射
            ├─ Reflex.php       Api 接口反射类
        ├─ response             接口响应
            ├─ Json.php         Api 接口响应类
        ├─ route                路由
            ├─ Route.php       Api 接口路由类
        ├─ validate            验证模型
            ├─ Api.php         Api 验证类
            ├─ Authorize.php   Api 权限验证类
            ├─ Host.php        Api 网址验证类
            ├─ Param.php       Api 接口参数验证类
            ├─ Route.php       Api 路由验证类
        ├─Api.php               API 基类 （继承这个类就好了）
        ├─Base.php              扩展Base类
        
api.php API restful 风格配置文件，主要用于配置一些默认值等

app.php API 项目配置文件，主要用于配置项目异常转发和是否开启debug模式。
~~~

#### 安装教程

> composer 安装

- 使用国内镜像
~~~
"repositories": {
    "packagist": {
      "type": "composer",
      "url": "https://packagist.phpcomposer.com"
    }
  }
~~~
- 安装 `china-wangyu/think-restful` 扩展
~~~
composer require china-wangyu/think-restful
~~~

#### 使用说明

本Restful API接口扩展包，使用的自建反射路由模式，默认格式为：

~~~
/**
* @doc 获取服务器授权1
* @route /api/v1/auth get
* @param string $appSecret 授权字符 require|alphaNum
* @param string $appSec2t 授权字符1 require|alphaNum
* @param string $appId 开发者ID
* @success {"code":400,"msg":"appSecret不能为空","data":[]}
* @error {"code":400,"msg":"appSecret不能为空","data":[]}
*/
public function read()
{
 return $this->success('成功~');
}
~~~

参数说明

| @ 名称 | 参数1注解 | 参数2注解 | 参数3注解 |
| :----: | :----: | :----: | :----: |
| doc  | API接口文档 | |  |
| route  | API路由规则 | 请求类型 |  |
| param  | api参数 | 验证规则 | 默认值 |
| success  | API请求成功返回json示例 |  |  |
| error  | API请求失败返回json示例 |  |  |

1. 必须是 **`PHP7.2`** 版本及以上

2. 必须是 **`TP5.1.0`** 版本以上

3. 首先用 **`composer`** 安装 **`china-wangyu/think-restful`**

4. 注册TP5.1路由

    > {项目}/route/route.php
    
    ~~~
    \think\restful\route\Route::register('api');
    ~~~
5. 配置扩展的 **`TP5.1`** 配置文件
    
    > 创建文件. **`{项目}/config/api.php`**
    
    内容：
    
    ```
    {扩展目录}/api.php 内容
    ```
    
    > 创建文件. **`{项目}/config/app.php`**
           
    内容：
    
     ```
        {扩展目录}/app.php 内容
     ```
     
6. 创建API基类，便于验证授权

    ```bash
    cp {扩展目录}/Base.php  {项目目录}/applicaition/api/controller/{接口版本}/Base.php
    ```

7. 配置 API 类继承 **`\app\api\controller\v1\Base`**

   例如：
   ~~~
   class Auth extends \app\api\controller\v1\Base
   ~~~

#### 获取当前 API 方法，过滤后的参数

 ~~~php
 $this->param;
 ~~~
     
#### 返回成功（json ）
 
> 注意本扩展某些函数与TP的\think\Controller的一些函数一样

~~~php
 return $this->success('成功~');
~~~
 
#### 返回失败（json ）
 
> 注意本扩展某些函数与TP的\think\Controller的一些函数一样

~~~
return $this->error('失败~');
~~~

### 开启 `china-wangyu/think-restful` 命令行

#### 命令行工具使用请先关闭`api.php`中`API_AUTHORIZATION`

设置`api.php`中`API_AUTHORIZATION=>false`

```
'API_AUTHORIZATION' => false,
```

#### 设置 TP5 命令行类

文件位置：**`{项目}/application/command.php`**
    
添加内容：
~~~
{扩展目录}/command.php 内容
~~~

- 打开当前位置命令行窗口

~~~bash
cd {项目目录}
~~~

- 检验命令行脚本是否输入成功

~~~  
{项目地址}> php think
Think Console version 0.1
 ....
 api
   api:check          验证 API 并生成 API Debug 文档
   api:make           生成 API 接口文档，markdown 格式

~~~
#### 输出接口文档 **``php think api:make ``**

~~~
{项目地址} > php think api:make

API markdown 接口文档地址: C:\Users\zhns_\Desktop\php\TP5.1-Restful-Api\\API接口文档2019-04-16 15.md

~~~
#### 输出接口自检  (有待改进)

> 我想这个功能我做的还不太完善，有待改进

- 首先需要开启一个本地网站服务
    
    - 使用PHP内置服务器
    ~~~
    php -S {IP地址}:{端口} -t {项目目录}/public/
    ~~~
    - TP5 启动服务
    ```
    > php think run -H {IP地址} -P {端口}
    
    ThinkPHP Development server is started On <http://127.0.0.1:8000/>
    You can exit with `CTRL-C`
    Document root is: E:\VirtualBox\vms\CICD\labs\tp5restfulapi_architecture\public
    ```
- 然后配置项目 `api.php` 配置文件的参数 `（可选）`

~~~
'API_HOST'=> 'http://127.0.0.1:8000',# 设置API网址
~~~

- 命令行运行脚本命令 **``php think api:check``**

> 如果没有执行上一步操作，请在这加上`-H `属性，并且设置属性值为：http://服务器网址:端口
例如：`php think api:check -H http://172.16.14.62:8000`
~~~
>  php think api:check -H http://172.16.14.62:8000

API markdown 自检文档地址: C:\Users\zhns_\Desktop\php\TP5.1-Restful-Api\\API自检文档2019-04-16 15.md
~~~

#### 开启权限验证

- 请开启 `api`配置文件的 `API_AUTHORIZATION` 属性为 `true`

```
// 是否开启授权验证
'API_AUTHORIZATION' => true,
```

- 创建授权类，如下

```bash
cp {扩展目录}/Token.php  {项目目录}/applicaition/api/controller/Token.php
```


#### 项目自评

本扩展或者说是一个TP5.1+PHP7.2的后端项目API架构，
主要是帮助刚刚入行或者快速建站的朋友们，进行项目快速迭代开发，
把接口授权、接口验证、参数校验、接口文档输出、接口自验包裹封装起来，
只为大家用的安心。

#### 帮助作者

项目开发或者扩展开发，都需要不断地编码尝试与线上环境验证。
所需的资源和时间都是有成本的，如果项目帮助到您了，
如果您有心帮助作者,请点击下方的捐赠按钮
    
#### 参与贡献

1. Fork 本仓库
2. 新建 ts_{用户名} 分支
3. 提交代码
4. 新建 Pull Request


#### 联系作者

 - 如有疑问，请联系邮箱 china_wangyu@aliyun.com

 - 请联系QQ 354007048 / 354937820